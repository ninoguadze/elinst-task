const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const TerserPlugin = require("terser-webpack-plugin");
const ESLintPlugin = require('eslint-webpack-plugin');

module.exports = {
    entry: "./src/index.js",
    output: {
        filename: "main.js",
        path: path.resolve(__dirname, "dist")
    },
    optimization: {
        minimize: true,
        minimizer: [new TerserPlugin()],
    },
    plugins: [new HtmlWebpackPlugin(
        {
            template: './src/index.html'
        }
    ), new ESLintPlugin()],
    mode: 'none',
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        port: 9000,
        hot: true,
    },
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"],
            },
            {
                test: /\.(png)$/i,
                type: 'asset/resource',
                use: { loader: "url-loader" }
            },
            {
                test: /\.png$/i,
                use: { loader: "file-loader" }
            },
            
            // {
            //     test: /\.(jpe?g|png)$/i,
            //     type: 'asset/resource',
            //     use: [
            //       {
            //         loader: 'file-loader',
            //       },
            //     ],
            //   },
        ],
    },
};