const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

export default function validate(email) {
    return VALID_EMAIL_ENDINGS.map((ending) => email.endsWith(ending)).includes(true) // eslint-disable-line max-len
};


